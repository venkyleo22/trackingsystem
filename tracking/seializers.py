from rest_framework.serializers import ModelSerializer
from tracking.models import Tracker, TrackingStatus, TrackerHistory


class TrackerSerializer(ModelSerializer):
    class Meta:
        model = Tracker
        fields = ['tracking_number','created_at']


class TrackerStatusSerializer(ModelSerializer):
    class Meta:
        model = TrackingStatus
        fields = '__all__'


class TrackerHistorySerializer(ModelSerializer):
    tracker = TrackerSerializer()
    statuses = TrackerStatusSerializer(many=True)

    class Meta:
        model = TrackerHistory
        fields = ['tracker','statuses']

