from django.contrib import admin
from tracking.models import TrackingStatus, Tracker, TrackerHistory
# Register your models here.

admin.site.register(TrackingStatus)
admin.site.register(Tracker)
admin.site.register(TrackerHistory)

