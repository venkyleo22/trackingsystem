from django.urls import path
from tracking.views import TrackerHistoryView, TrackerCreateView

urlpatterns = [
    path('create/', TrackerCreateView.as_view(), name='Create_Tracking'),
    path('<int:tracking_id>/',TrackerHistoryView.as_view(), name='tracking_history'),
    path('update/<int:tracking_id>/', TrackerHistoryView.as_view(), name='Update_Tracking'),
]
