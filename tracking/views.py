from django.shortcuts import render
from django.views import View
from django.http.response import JsonResponse

from tracking.models import TrackingStatus, Tracker, TrackerHistory
from tracking.services import TrackerGeneratorService, IDGenerator
from tracking.seializers import TrackerHistorySerializer


class TrackerCreateView(View):

    def post(self, request):
        id_generator = IDGenerator()
        status = TrackingStatus.Choices.SHIPPED
        tracker = TrackerGeneratorService.generate_tracker(id_generator.generate_id(), status)
        tracker.process()
        return JsonResponse({'id':tracker.tracker_id}, status=201)


class TrackerHistoryView(View):

    def get_tracker(self, params):
        tracking_id = params.get('tracking_id')
        return Tracker.objects.get(tracking_number=tracking_id)

    def get(self, request, *args, **params):
        tracker = self.get_tracker(params)
        tracker_history = TrackerHistory.objects.get(tracker=tracker)
        serializer = TrackerHistorySerializer(tracker_history)
        return JsonResponse(serializer.data, status=200)

    def post(self, request, *args, **params):
        # create history
        pass
