from django.db.models import (
    BigIntegerField,
    CASCADE,
    CharField,
    ForeignKey,
    DateTimeField,
    ManyToManyField,
    Model,
    OneToOneField,
    TextChoices
)


class Tracker(Model):
    tracking_number = BigIntegerField(primary_key=True)
    created_at = DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.tracking_number)


class TrackingStatus(Model):
    class Choices(TextChoices):
        SHIPPED = "SHIPPED", "SHIPPED"
        IN_PROGRESS = "IN_PROGRESS", "In_Progress"
        OUT_FOR_DELIVERY = "OUT_FOR_DELIVERY", "Out_For_Delivery"
        DELIVERED = "DELIVERED", "Delivered"

    status = CharField(max_length=50, choices=Choices.choices)
    created_at = DateTimeField(auto_now_add=True)
    comment = CharField(max_length=30, null=True, blank=True)

    def __str__(self):
        return str(self.status)


class TrackerHistory(Model):
    tracker = OneToOneField(Tracker, on_delete=CASCADE)
    statuses = ManyToManyField(TrackingStatus)

    def __str__(self):
        return str(self.tracker.tracking_number)