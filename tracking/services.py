import time

from tracking.models import Tracker, TrackingStatus, TrackerHistory


class IDGenerator:
    _instance = None
    _counter = 0

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(IDGenerator, cls).__new__(cls)
            cls._counter = 1
        cls._counter +=1
        return cls._instance

    def generate_id(self):
        # custom id generation
        timestamp = int(time.time() * 1000)
        return int(str(self._counter) + str(timestamp))


class TrackerGeneratorService:

    def __init__(self, tracker_id, status):
        self.tracker_id = tracker_id
        self.tracker_status = status
        self.tracker = None

    def tracker_history(self, status):
        ts = TrackingStatus.objects.create(status=status)
        th = TrackerHistory.objects.create(tracking_number=self.tracker)
        th.save()
        th.statuses.add(ts)
        th.save()

    def create_tracker(self):
        self.tracker = Tracker.objects.create(tracking_number=self.tracker_id)
        self.tracker.save()

    def process(self):
        self.create_tracker()
        self.tracker_history(self.tracker_status)

    @classmethod
    def generate_tracker(cls, tracker_id, status):
        return TrackerGeneratorService(tracker_id, status)

